import euler

euler.install_thrift_import_hook()

from idl.user_thrift import UserService, CreateUserRequest, User, base, GetUserRequest, UpdateUsernameRequest,\
                            UpdateAvatarRequest

url_boe = "sd://ad.audit.kjb_entrytask?cluster=default"

url_local = "tcp://127.0.0.1:8888"

rpc_client = euler.Client(UserService, url_local)


def create_user(username, account, password, avatar):
    user = User(username=username, account=account, password=password, avatar=avatar)
    req = CreateUserRequest(user)
    resp = rpc_client.CreateUser(req)
    return resp


def get_user(account, password):
    req = GetUserRequest(account, password)
    resp = rpc_client.GetUser(req)
    return resp


def update_username(account, username):
    req = UpdateUsernameRequest(account=account, username=username)
    resp = rpc_client.UpdateUsername(req)
    return resp


def update_avatar(account, avatar):
    req = UpdateAvatarRequest(account=account, avatar=avatar)
    resp = rpc_client.UpdateAvatar(req)
    return resp


if __name__ == '__main__':
    # print(create_user("123", "byce", "bytedance678", "dhiowahdlwiahdiawnkldwhaklhdiawdaw"))
    # print(get_user("bytedance", "bytedance678"))
    # print(update_username("bytedance","fty555"))
    print(update_avatar("bytedance","sadasd"))
