include "base.thrift"

namespace go ad.audit.fty.entrytask
namespace py ad.audit.fty.entrytask

struct User {
    1: optional i64 id,
    2: optional string account,
    3: optional string password,
    4: optional string username,
    5: optional binary avatar,

}

struct GetUserRequest {
    1: required string account,
    2: required string password,
}

struct GetUserResponse {
    1: required User user,
    2: optional base.BaseResp BaseResp,
}

struct CreateUserRequest{
    1: required User user,
}

struct CreateUserResponse{
    1: optional base.BaseResp BaseResp,
}

struct GetUserByAccountRequest {
    1: required string account,
}

struct GetUserByAccountResponse {
    1: required User user,
}

struct UpdateUsernameRequest{
    1: required string account,
    2: required string username,
}

struct UpdateUsernameResponse{
   1: optional base.BaseResp BaseResp,
}

struct UpdateAvatarRequest{
    1: required string account,
    2: required binary avatar,
}

struct UpdateAvatarResponse{
    1: optional base.BaseResp BaseResp,
}

service UserService {
    GetUserResponse GetUser (1: GetUserRequest req),
    CreateUserResponse CreateUser(1: CreateUserRequest req),
    GetUserByAccountResponse GetUserByAccount(1: GetUserByAccountRequest req),
    UpdateUsernameResponse UpdateUsername(1: UpdateUsernameRequest req),
    UpdateAvatarResponse UpdateAvatar(1: UpdateAvatarRequest req),
}

