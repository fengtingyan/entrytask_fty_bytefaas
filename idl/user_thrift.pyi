# coding:utf-8
from . import base_thrift as base


# noinspection PyPep8Naming, PyShadowingNames
class User(object):
    id: int
    username: str
    password: str
    account: str
    avatar: str

    def __init__(self,
                 id: int = None,
                 username: str = None,
                 password: str = None,
                 account: str = None,
                 avatar: str = None) -> None:
        ...


# noinspection PyPep8Naming, PyShadowingNames
class GetUserRequest(object):
    account: str
    password: str
    def __init__(self, account: str, password: str) -> None:
        ...

# noinspection PyPep8Naming, PyShadowingNames
class GetUserResponse(object):
    User: User
    BaseResp: base.BaseResp

    def __init__(self,
                 User: User = None,
                 BaseResp: base.BaseResp = None) -> None:
        ...

# noinspection PyPep8Naming, PyShadowingNames
class CreateUserRequest(object):
    User: User

    def __init__(self, User: User = None) -> None:
        ...


# noinspection PyPep8Naming, PyShadowingNames
class CreateUserResponse(object):
    BaseResp: base.BaseResp

    def __init__(self, BaseResp: base.BaseResp = None) -> None:
        ...

# noinspection PyPep8Naming, PyShadowingNames
class GetUserByAccountRequest(object):
    account: str

    def __init__(self, account: str) -> None:
        ...


# noinspection PyPep8Naming, PyShadowingNames
class GetUserByAccountResponse(object):
    User: User

    def __init__(self, User: User) -> None:
        ...

# noinspection PyPep8Naming, PyShadowingNames
class UpdateUsernameRequest(object):
    account: str
    username: str

    def __init__(self, account: str, username: str) -> None:
        ...

# noinspection PyPep8Naming, PyShadowingNames
class UpdateUsernameResponse(object):
    Base: base.BaseResp

    def __init__(self, Base: base.BaseResp) -> None:
        ...

# noinspection PyPep8Naming, PyShadowingNames
class UpdateAvatarRequest(object):
    account: str
    avatar: str

    def __init__(self, account: str, avatar: str) -> None:
        ...

# noinspection PyPep8Naming, PyShadowingNames
class UpdateAvatarResponse(object):
    Base: base.BaseResp

    def __init__(self, Base: base.BaseResp) -> None:
        ...


# noinspection PyPep8Naming, PyShadowingNames
class UserService(object):

    def GetUser(self, req: GetUserRequest = None) -> GetUserResponse:
        ...

    def CreateUser(self, req: CreateUserRequest = None) -> CreateUserResponse:
        ...

    def GetUserByAccount(self, req: GetUserByAccountRequest) -> GetUserByAccountResponse:
        ...

    def UpdateUsername(self, req: UpdateUsernameRequest) -> UpdateUsernameResponse:
        ...

    def UpdateAvatar(self, req: UpdateAvatarRequest = None) -> UpdateAvatarResponse:
        ...



