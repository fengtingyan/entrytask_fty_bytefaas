# coding:utf-8
from typing import Dict


# noinspection PyPep8Naming, PyShadowingNames
class TrafficEnv(object):
    Open: bool
    Env: str

    def __init__(self, Open: bool = False, Env: str = '') -> None:
        ...


# noinspection PyPep8Naming, PyShadowingNames
class Base(object):
    LogID: str
    Caller: str
    Addr: str
    Client: str
    TrafficEnv: TrafficEnv
    Extra: Dict[str, str]

    def __init__(self,
                 LogID: str = '',
                 Caller: str = '',
                 Addr: str = '',
                 Client: str = '',
                 TrafficEnv: TrafficEnv = None,
                 Extra: Dict[str, str] = None) -> None:
        ...


# noinspection PyPep8Naming, PyShadowingNames
class BaseResp(object):
    StatusMessage: str
    StatusCode: int
    Extra: Dict[str, str]

    def __init__(self,
                 StatusMessage: str = '',
                 StatusCode: int = 0,
                 Extra: Dict[str, str] = None) -> None:
        ...
